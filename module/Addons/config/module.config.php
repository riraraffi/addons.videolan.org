<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonAddons for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __DIR__ . '/../public',
            ),
        ),
    ),

    'router' => array(
        'routes' => array(

//            'home' => array(
//                'type' => 'Zend\Mvc\Router\Http\Literal',
//                'options' => array(
//                    'route' => '/',
//                    'defaults' => array(
//                        'controller' => 'Addons\Controller\Index',
//                        'action' => 'index',
//                    ),
//                ),
//            ),

            'addonslisting' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Addons\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,

                'child_routes' => array(

                    /* View Filters */
                    'typefilter' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[type/:type/][order/:order]',
                            'constraints' => array(
                                'type' => '(extension|skin|playlist|discovery|other|interface|meta)',
                                'order' => '(downloads|name|)',
                            ),
                            'defaults' => array('type'=>'', 'order'=>''),
                        ),
                    ),

                    /* XML Listing */
                    'xmllisting' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'xml',
                            'defaults' => array(
                                'action' => 'xml',
                            ),
                        ),
                    ),


                ),
            ),

            'addondetails' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '[/:uuid]',
                    'constraints' => array(
                         'uuid' => '[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Addons\Controller',
                        'controller' => 'Details',
                        'action' => 'index',
                    ),
                ),

                'may_terminate' => true,

                'child_routes' => array(

                    /* Download Link */
                    'download' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/download',
                            'defaults' => array(
                                'action' => 'download'
                            ),
                        ),
                    ),

                    /* Image */
                    'image' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/image',
                            'defaults' => array(
                                'action' => 'image'
                            ),
                        ),
                    ),

                    /* Edit Link */
                    'edit' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/edit',
                            'defaults' => array(
                                'controller' => 'Edition',
                                'action' => 'edit'
                            ),
                        ),
                    ),

                    /* EditDesc Link */
                    'editdesc' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/editdesc',
                            'defaults' => array(
                                'controller' => 'Edition',
                                'action' => 'editdesc'
                            ),
                        ),
                    ),

                    /* Edit Link */
                    'editfiles' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/editfiles',
                            'defaults' => array(
                                'controller' => 'Edition',
                                'action' => 'editfiles'
                            ),
                        ),
                    ),

                    /* Edit Link */
                    'ajax' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/ajax/:query',
                            'defaults' => array(
                                'controller' => 'Edition',
                                'action' => 'ajax'
                            ),
                        ),
                    ),

                 ),//!child_routes

            ),//!addondetails


            'addoncreator' => array(
                'type' => 'Literal',
                'priority' => 1001,
                'options' => array(
                    'route' => '/user',
                    'defaults' => array(
                        'controller' => 'zfcuser',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    /* List Link */
                    'list' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/list',
                            'defaults' => array(
                                'controller' => 'Addons\Controller\Edition',
                                'action' => 'list'
                            ),
                        ),
                    ),

                    /* New */
                    'new' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/new',
                            'defaults' => array(
                                'controller' => 'Addons\Controller\Edition',
                                'action' => 'new'
                            ),
                        ),
                    ),

                    /* Upload Link */
                    'upload' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/upload',
                            'defaults' => array(
                                'controller' => 'Addons\Controller\Edition',
                                'action' => 'upload'
                            ),
                        ),
                    ),
                 ),//!child_routes
            ),



            'admin' => array(

                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Addons\Controller',
                        'controller' => 'Admin',
                        'action' => 'index',
                    ),
                ),

                'may_terminate' => true,

                'child_routes' => array(

                    /* View Filters */
                    'filter' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[type/:type/][state/:state]',
                            'constraints' => array(
                                'type' => '(extension|skin|playlist|discovery|other|interface|meta)',
                                'state' => '(waiting|valid|)',
                            ),
                            'defaults' => array('type'=>'', 'state'=>''),
                        ),
                    ),

                    /* View Filters */
                    'inspect' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[/:uuid]',
                            'constraints' => array(
                                'uuid' => '[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}',
                            ),
                            'defaults' => array(
                                'action' => 'inspect'
                            ),
                        ),
                    ),

                )

            ),

            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
//                    'default' => array(
//                        'type' => 'Segment',
//                        'options' => array(
//                            'route' => '/[:controller[/:action]]',
//                            'constraints' => array(
//                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                            ),
//                            'defaults' => array(),
//                        ),
//                    ),


        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
              'navigation' => function($sm) {
                    $navigation = new \Zend\Navigation\Service\DefaultNavigationFactory;
                    \Zend\Navigation\Page\Mvc::setDefaultRouter($sm->get('router'));
                    return $navigation->createService($sm);
              },
            'OwnerAssertion' => function($sm){
                  $zfid = $sm->get('zfcuser_user_service')->getAuthService();
                  //$zfid->getIdentity()->getId();
                  $assertion = new \Addons\Acl\Assertion\OwnerAssertion();
                  $assertion->identity = $zfid->getIdentity();
                  return $assertion;
            }
        ),
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator'
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Addons\Controller\Index' => 'Addons\Controller\IndexController',
            'Addons\Controller\Edition' => 'Addons\Controller\EditionController',
            'Addons\Controller\Details' => 'Addons\Controller\DetailsController',
            'Addons\Controller\Admin' => 'Addons\Controller\AdminController',
        ),
    ),

    'view_manager' => array(
        'strategies' => array (
            'ViewJsonStrategy',
        ),
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            //'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),


    'navigation' => array(
        'default' => array(
            array(
                'id'     => 'IndexRoot',
                'label'  => 'Index',
                'route' => 'addonslisting',
            ),

            array(
                'id'    => 'UserRoot',
                'label' => 'My Account',
                'route' => 'zfcuser/login',
                'pages' => array(
                    array(
                        'id'    => 'UserAddonsRoot',
                        'label' => 'My Addons',
                        'route' => 'addoncreator/list',
                        'pages' => array(
                            array(
                                'label' => 'Create new',
                                'route' => 'addoncreator/new',
                            ),
                            array(
                                'label' => 'Upload VLP distribution archive',
                                'route' => 'addoncreator/upload',
                            )
                        )
                    ),

                )
            ),

            array(
                'id'     => 'AdminRoot',
                'label'  => 'Admin',
                'route' => 'admin',
            ),

        )

    ),


);
