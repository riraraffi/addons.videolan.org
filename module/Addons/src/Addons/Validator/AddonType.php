<?php

namespace Addons\Validator;
use Zend\Validator\AbstractValidator;

class AddonType extends AbstractValidator
{
    const OUT_OF_RANGE = 0;
    var $values = array('extension', 'skin', 'playlist', 'discovery', 'interface', 'meta', 'other');

    protected $messageTemplates = array(
        self::OUT_OF_RANGE => "'%value%' is an invalid addon type"
    );
   
    public function isValid($value)
    {
        if (!in_array($value, $this->values))
        {
            $this->setValue($value);
            $this->error(self::OUT_OF_RANGE);
            return false;
        }
        return true;
    }
}
