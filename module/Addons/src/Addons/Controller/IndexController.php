<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Addons\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \DOMDocument;

class IndexController extends AbstractActionController
{
    protected $addonsTable;

    public function indexAction()
    {
        $paginator = $this->getAddonsTable()->fetch( $this->params('type'), $this->params('order'), $this->params()->fromQuery('lookup', null), true, 'valid' );
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));

        $paginator->setItemCountPerPage( ( $this->params('type') == "skin" ) ? 16 : 10 );

        return new ViewModel( array( 'paginator' => $paginator,
                                     'addons' => $paginator,
                                     'type' => $this->params('type'),
                                     'order' => $this->params('order') ) );
    }

    private function addAuthorShip(&$data)
    {
        if(!empty($data['ref_userid']))
        {
            $zfcmapper = $this->getServiceLocator()->get('zfcuser_user_mapper');
            $user = $zfcmapper->findById( $data['ref_userid'] );
            $data['email'] = $user->getEmail();
            $data['creator'] = $user->getUsername();
        }
    }

    public function xmlAction()
    {
        $alladdons = $this->getAddonsTable()->fetch( null, 'name', null, false, 'valid' );

        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->formatOutput = true;
        $root = $dom->createElementNS( 'http://videolan.org/ns/vlc/addons/1.0', 'videolan' );

        $desc = $dom->createElement( "addons" );

        $addonhydrator = new \Zend\Stdlib\Hydrator\ObjectProperty;
        $addonnodehydrator = new \Addons\Mapper\XMLAddonNodeHydrator;

        foreach ($alladdons as $add)
        {
            $addonnode = $dom->createElement("addon");

            /*FIXME: have resultset hydrate directly nodes*/
            $data = $addonhydrator->extract($add);
            $this->addAuthorShip($data);
            $addonnodehydrator->hydrate($data, $addonnode);

            $r = $dom->createElement( "archive", $this->url()->fromRoute('addondetails/download', ['uuid' => $add->uuid]) );
            $addonnode->appendChild( $r );

            $desc->appendChild($addonnode);
        }

        $root->appendChild( $desc );
        $dom->appendChild( $root );

        $viewModel = new ViewModel();
        $viewModel->setTerminal( true );
        $this->getResponse()->setContent( $dom->saveXML() );
        $this->getResponse()->getHeaders()->addHeaders( array('Content-type' => 'text/xml') );
        return $this->getResponse();
    }

    public function getAddonsTable()
    {
        if (!$this->addonsTable) {
            $sm = $this->getServiceLocator();
            $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
        }
        return $this->addonsTable;
    }

}
