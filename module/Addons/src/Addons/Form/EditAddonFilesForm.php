<?php

namespace Addons\Form;

use Zend\Form\Element;
use ZfcBase\Form\ProvidesEventsForm;
use Addons\Model\Addon;
use Zend\Filter;
use Zend\InputFilter\Input;
use Zend\InputFilter\FileInput;

class EditAddonFilesForm extends ProvidesEventsForm
{
    public function __construct()
    {
        parent::__construct();

        $this->add(array(
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'resources',
            'options' => array(
                'label' => 'Addons files',
                'count' => 1,
                'should_create_template' => true,
                'template_placeholder' => '__placeholder__',
                'target_element' => array(
                    'type' => 'Addons\Form\FileFieldset'
                )
            )
        ));

        $submitElement = new Element\Button('submit');
        $submitElement
            ->setLabel('Commit changes')
            ->setAttributes(array(
                'type'  => 'submit',
            ));
        $this->add($submitElement, array(
            'priority' => -100,
        ));



    }
}
