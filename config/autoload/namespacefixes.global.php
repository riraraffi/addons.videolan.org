<?php

Zend\Loader\AutoloaderFactory::factory(array(
     'Zend\Loader\StandardAutoloader' => array(
         'namespaces' => array(
             'Assetic' => __DIR__ . '/../../vendor/Assetic/src/Assetic',
         ),
     )
));

return array();